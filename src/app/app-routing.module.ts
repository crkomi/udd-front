import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { HomeComponent } from './home/home.component';
import { PasswordComponent } from './password/password.component';
import { UsersComponent } from './users/users.component';
import { UserComponent } from './users//user/user.component';
import { AddUserComponent } from './users/adduser/add-user.component';
import { CategoriesComponent } from './categories/categories.component';
import { CategoryComponent } from './categories/category/category.component';
import { AddCategoryComponent } from './categories/addcategory/add-category.component';
import { EbooksComponent } from './ebooks/ebooks.component';
import { EbookComponent } from './ebooks/ebook/ebook.component';
import { AddEbookComponent } from './ebooks/addebook/add-ebook.component';
import { SearchComponent } from './search/search.component';

//import { TasksComponent } from './tasks/tasks.component';
//import { RecipesComponent } from './recipes/recipes.component';
//import { TaskDetailComponent } from './tasks/task-detail/task-detail.component';
//import {SignupComponent} from "./auth/signup/signup.component";
//import {SigninComponent} from "./auth/signin/signin.component";

const appRoutes: Routes = [
	//{ path:'', redirectTo:'/tasks', pathMatch:'full'},
	{ path:'search', component: SearchComponent},
  { path:'users', component: UsersComponent, children:[
		//{path:'', component: RecipeStartComponent},
		//{path:'new', component: RecipeEditComponent,canActivate: [AuthGuard]},
		{path:'add', component: AddUserComponent},
    {path:':id', component: UserComponent}

		//{path:':id/edit', component: RecipeEditComponent,canActivate: [AuthGuard]}
	]},{ path:'categories', component: CategoriesComponent, children:[
    //{path:'', component: RecipeStartComponent},
    //{path:'new', component: RecipeEditComponent,canActivate: [AuthGuard]},
    {path:'add', component: AddCategoryComponent},
    {path:':id', component: CategoryComponent}

    //{path:':id/edit', component: RecipeEditComponent,canActivate: [AuthGuard]}
  ]},{ path:'ebooks', component: EbooksComponent, children:[
    //{path:'', component: RecipeStartComponent},
    //{path:'new', component: RecipeEditComponent,canActivate: [AuthGuard]},
    {path:'add', component: AddEbookComponent},
    {path:':id', component: EbookComponent}

    //{path:':id/edit', component: RecipeEditComponent,canActivate: [AuthGuard]}
  ]},
  	{ path: 'login', component: LoginComponent},
  	{ path: 'profile', component: ProfileComponent},
  	{ path: 'home', component: HomeComponent},
  	{ path: 'password', component: PasswordComponent},
  //	{ path: 'users', component: UsersComponent}
  //{ path: 'signin', component: SigninComponent}
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule{

}
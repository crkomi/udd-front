import { Category } from './category.model';
import { Language } from './language.model';

export class Ebook{

	public id?:number;
    public title?:string;
    public author?:string;
    public keywords?:string;
    public publicationYear?:number;
    public fileName?:string;
    public mime?:string;
    public categoryEntity?:Category;
    public languageEntity?:Language;
    public ebookDownloadLink?:string;
    public highlight?:string;

	constructor(id:number,title:string, author:string, keywords:string, publicationYear:number, fileName:string, mime:string, categoryEntity:Category, languageEntity:Language){
		this.id = id;
		this.title = title;
		this.author = author;
		this.keywords = keywords;
		this.publicationYear = publicationYear;
		this.fileName = fileName;
		this.mime = mime;
		this.categoryEntity = categoryEntity;
		this.languageEntity = languageEntity;
	}
	
}
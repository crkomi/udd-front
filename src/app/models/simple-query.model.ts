
export class SimpleQuery{

	public value?:string;
    public field?:string;

	constructor(value:string,field:string){
		this.value = value;
		this.field = field;
	}
	
}
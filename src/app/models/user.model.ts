import { Category } from './category.model';

export class User{

	public id?:number;
    public firstname?:string;
    public lastname?:string;
    public username?:string;
    public password?:string;
    public type?:string;
    public categoryEntity?:Category;

	constructor(id:number,firstname:string, lastname:string, username:string, password:string, type:string, categoryEntity:Category){
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.username = username;
		this.password = password;
		this.type = type;
		this.categoryEntity = categoryEntity;
	}

	//public cloneFunction(test:boolean){
	//	return new User(this.id, this.firstname, this.lastname, this.username, this.password, this.type, this.categoryEntity.cloneFunction());
	//}
	
}
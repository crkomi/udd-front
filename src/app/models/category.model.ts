import { Ebook } from './ebook.model';

export class Category{

	public id?:number;
    public name?:string;
    public eBookEntityList?:Ebook[];

	constructor(id:number,name:string){
		this.id = id;
		this.name = name;
		this.eBookEntityList = [];
	}
	
}
import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { Category } from '../models/category.model';
import  { ServerService } from '../server.service';

@Component({
	selector : 'app-profile',
  styleUrls: ['./profile.component.css'],
	templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  user:User;
  oldUser:User;
  onlyRead:boolean;
  
  constructor(private router: Router, private serverService:ServerService) {}

  ngOnInit() {
    this.user = this.serverService.currentUser;
    this.oldUser =  this.cloneUser(this.user);
    this.onlyRead = false;
  }

  onFetchData(){
    
  }

  onLogout(){
    
  }
  
  onSubmit(){
    console.log("do edit");
    
    this.serverService.editUser(this.user).subscribe(
      (response: Response) => {
        console.log("Success edit");
          if(response.statusText === "OK"){
            this.oldUser =  this.cloneUser(this.user)
              toastr.success("Success edit"); 
           }else{
              
           }
      },error => {
            toastr.error("Incorrect username");
        }
     );
    
  }

  refresh(){
    this.user = this.cloneUser(this.oldUser);
  }

  cloneUser(user:User){

    return new User(user.id, user.firstname, user.lastname, user.username, user.password, user.type, new Category(user.categoryEntity.id, user.categoryEntity.name));
  }

}
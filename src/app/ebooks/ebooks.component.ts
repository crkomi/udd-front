import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { Ebook } from '../models/ebook.model'
import { Category } from '../models/category.model'
import  { ServerService } from '../server.service';
import  { DataService } from '../data.service';

@Component({
	selector : 'app-ebooks',
  styleUrls: ['./ebooks.component.css'],
	templateUrl: './ebooks.component.html'
})
export class EbooksComponent implements OnInit {

  ebooks:Ebook[];
  categories:Category[];
  selectedCategory:string;
  
  constructor(private router: Router, private serverService:ServerService, private dataService:DataService) {}

  ngOnInit() {
    this.ebooks = this.dataService.ebooks;
    this.dataService.categories.splice(0, this.dataService.categories.length);
    this.categories = this.dataService.categories;
    this.ebooks.splice(0, this.ebooks.length);
    this.selectedCategory = this.dataService.selectedCategory;
    
    let x=this;
    this.serverService.getAllCategories().subscribe(
           (response: Response) => {
              for (var i = response.json().length - 1; i >= 0; i--) {
                x.categories.push(new Category(response.json()[i].id, response.json()[i].name));
              }
              for (var i = x.categories.length - 1; i >= 0; i--) {
                  let k = i;
                  this.serverService.getEbooks(x.categories[i].id).subscribe(
                   (response1: Response) => {
                      for (var j = response1.json().length - 1; j >= 0; j--) {
                           x.categories[k].eBookEntityList.push(response1.json()[j]);
                       }  
                   }
                  );
                
              }
           }
       );
  }

  onFetchData(){
    
  }

  onLogout(){
    
  }
  onSubmit(){

  }

  clickCategory(){
    this.ebooks.splice(0, this.ebooks.length);
    this.dataService.selectedCategory = this.selectedCategory;
    for (var i = this.categories.length - 1; i >= 0; i--) {
      if(this.selectedCategory === this.categories[i].name || this.selectedCategory === "All"){
        for (var j = this.categories[i].eBookEntityList.length - 1; j >= 0; j--) {
          this.ebooks.push(this.categories[i].eBookEntityList[j]);
        }
      }
    }
  }

  refreshTree(){
    this.ebooks.splice(0, this.ebooks.length);
    this.categories.splice(0, this.categories.length);
    let x=this;
     this.serverService.getAllCategories().subscribe(
             (response: Response) => {
                for (var i = response.json().length - 1; i >= 0; i--) {
                  x.categories.push(new Category(response.json()[i].id, response.json()[i].name));
                }
                for (var i = x.categories.length - 1; i >= 0; i--) {
                    let k = i;
                    this.serverService.getEbooks(x.categories[i].id).subscribe(
                     (response1: Response) => {
                        for (var j = response1.json().length - 1; j >= 0; j--) {
                             x.categories[k].eBookEntityList.push(response1.json()[j]);
                         }  
                     }
                    );
                  
                }
                x.clickCategory();
             }
         );
  }

}
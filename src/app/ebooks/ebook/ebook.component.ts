import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ServerService } from '../../server.service';
import { DataService } from '../../data.service';
import { Response } from '@angular/http'; 
import { NgForm } from '@angular/forms';
import { Ebook } from '../../models/ebook.model';
import { Language } from '../../models/language.model';
import { Category } from '../../models/category.model';

@Component({
  selector: 'app-ebook',
  templateUrl: './ebook.component.html',
  styleUrls: ['./ebook.component.css']
})
export class EbookComponent implements OnInit {
  ebook: Ebook;
  ebookLinkDownload:string;
  id: string;
  oldEbook:Ebook;
  onlyRead:boolean;
  categories:Category[];
  languages:Language[];
  file:File;

  constructor(private route: ActivatedRoute, private router: Router, private serverService:ServerService, private dataService:DataService) { }

  ngOnInit() {
    this.categories = this.dataService.categories;
    this.ebook = new Ebook(-1,"","","",1,"","", new Category(-1,""), new Language(-1,""));
      this.route.params.subscribe(
        (params: Params) => {
           this.id = params['id'];

           console.log(this.id);
           this.serverService.getBook(this.id).subscribe(
              (response: Response) => {
                console.log(response);
                this.ebook = response.json();
                this.oldEbook =  this.cloneEbook(this.ebook);
                this.onlyRead = false;
                let tempArray:string[] = this.ebook.fileName.split('\\');
                this.ebookLinkDownload = "http://localhost:8080/" + tempArray[tempArray.length - 1];
                console.log("Donwload link");
                console.log(this.ebookLinkDownload);
              }
          );
      });

      this.serverService.getAllLanguages().subscribe(
              (response: Response) => {
                console.log(response);
                this.languages = response.json();
              }
          );
  }

  isDownloadPdf(){
        if(this.serverService.isAdmin() || ( this.serverService.isSubscriber() && (this.serverService.currentUser.categoryEntity.name === this.ebook.categoryEntity.name || this.serverService.currentUser.categoryEntity.name == "All"))){
          return true;
        }
        return false;
  }

   onSubmit(){
     
    console.log("do edit");
    this.ebook.categoryEntity.id = this.getIdFromCategoryName(this.ebook.categoryEntity.name);
    this.ebook.languageEntity.id = this.getIdFromLanguageName(this.ebook.languageEntity.name);

    this.serverService.editEbook(this.ebook).subscribe(
      (response: Response) => {
        console.log("Success edit");
          if(response.statusText === "OK"){
            this.oldEbook =  this.cloneEbook(this.ebook);
              let tempEbook:Ebook = response.json();
              let oldId=null;
              for (var i = this.dataService.ebooks.length - 1; i >= 0; i--) {
                if(this.dataService.ebooks[i].id === response.json().id){
                  //this.dataService.ebooks[i] = this.cloneEbook(tempEbook);
                  if(this.dataService.ebooks[i].categoryEntity.id === tempEbook.categoryEntity.id || this.dataService.selectedCategory === "All"){
                    this.copyEbook(tempEbook, this.dataService.ebooks[i]);
                  }else{
                    oldId = this.dataService.ebooks[i].categoryEntity.id;
                    this.dataService.ebooks.splice(i, 1);
                  }
                  break;
                } 

           }
           if(oldId == null){
                    for (var i = this.categories.length - 1; i >= 0; i--) {
                     if(this.categories[i].name === response.json().categoryEntity.name){
                       for (var j = this.categories[i].eBookEntityList.length - 1; j >= 0; j--) {
                         if(this.categories[i].eBookEntityList[j].id === response.json().id){
                           //this.categories[i].eBookEntityList[j] = this.cloneEbook(tempEbook);
                           this.copyEbook(tempEbook, this.categories[i].eBookEntityList[j]);
                           break;
                         }
                       }
                     }
                    } 
                 }else{
                   for (var i = this.categories.length - 1; i >= 0; i--) {
                     if(this.categories[i].id === oldId){
                       for (var j = this.categories[i].eBookEntityList.length - 1; j >= 0; j--) {
                         if(this.categories[i].eBookEntityList[j].id === oldId){
                           this.categories[i].eBookEntityList.splice(j,1);
                         }
                       }
                     }else if(this.categories[i].id == tempEbook.categoryEntity.id){
                         this.categories[i].eBookEntityList.push(tempEbook);
                     }
                   }
                 }
           toastr.success("Success edit");
         }
      },error => {
            toastr.error("Incorrect username");
        }
     );
    
  }

  copyEbook(fromBook:Ebook, toBook:Ebook){
   //Ebook(ebook.id, ebook.title, ebook.author, ebook.keywords, ebook.publicationYear, ebook.fileName, ebook.mime
   //   , new Category(ebook.categoryEntity.id, ebook.categoryEntity.name), new Language(ebook.languageEntity.id, ebook.languageEntity.
    toBook.id = fromBook.id;
    toBook.title = fromBook.title;
    toBook.author = fromBook.author;
    toBook.keywords = fromBook.keywords;
    toBook.publicationYear = fromBook.publicationYear;
    toBook.fileName = fromBook.fileName;
    toBook.mime = fromBook.mime;
    toBook.categoryEntity.id = fromBook.categoryEntity.id;
    toBook.categoryEntity.name = fromBook.categoryEntity.name;
    toBook.languageEntity.id = fromBook.languageEntity.id;
    toBook.languageEntity.name = fromBook.languageEntity.name;
  }

  uploadEbook(){
    console.log(this.file);
    this.serverService.uploadFile(this.file).subscribe(
              (response: Response) => {
                console.log(response);
                console.log("Uspesno uploadovan file");
                let responseEbook: Ebook = response.json();
                this.ebook.fileName = responseEbook.fileName;
                this.ebook.author = responseEbook.author;
                this.ebook.title = responseEbook.title;
                this.ebook.keywords = responseEbook.keywords;
                this.ebook.mime = responseEbook.mime;
              }
          );
  }

  getIdFromCategoryName(categoryName:string){
    for (var i = this.categories.length - 1; i >= 0; i--) {
      if(this.categories[i].name === categoryName){
        return this.categories[i].id;
      }
    }
  }

  getIdFromLanguageName(languageName:string){
    for (var i = this.languages.length - 1; i >= 0; i--) {
      if(this.languages[i].name === languageName){
        return this.languages[i].id;
      }
    }
  }

  refresh(){
    this.ebook = this.cloneEbook(this.oldEbook);
  }

  cloneEbook(ebook:Ebook){
    return new Ebook(ebook.id, ebook.title, ebook.author, ebook.keywords, ebook.publicationYear, ebook.fileName, ebook.mime, new Category(ebook.categoryEntity.id, ebook.categoryEntity.name), new Language(ebook.languageEntity.id, ebook.languageEntity.name));
  }

  delete(){
    
    this.serverService.deleteEbook(this.ebook.id).subscribe(
      (response: Response) => {
        console.log("Success delete");
          if(response.statusText === "OK"){
            toastr.success("Success delete");
            for (var i = this.dataService.ebooks.length - 1; i >= 0; i--) {
                if(this.dataService.ebooks[i].id === response.json().id){
                  this.dataService.ebooks.splice(i,1);
                  break;
                }
              }
              for (var i = this.categories.length - 1; i >= 0; i--) {
               if(this.categories[i].name === response.json().categoryEntity.name){
                 for (var j = this.categories[i].eBookEntityList.length - 1; j >= 0; j--) {
                   if(this.categories[i].eBookEntityList[j].id === response.json().id){
                     this.categories[i].eBookEntityList.splice(j,1);
                   }
                 }
               }
             }  
            this.router.navigate(['../'], { relativeTo: this.route});
           }
      },error => {
            toastr.error("Failed delete");
        }
     );
  }

  fileEvent(e){
    //console.log(e.target.files[0]);

    this.file = e.target.files[0];
    console.log(this.file);
    //this.ebook.title = this.file.name;
  }

  checkUpload(){
    return this.file == undefined;
  }



}

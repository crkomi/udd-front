import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ServerService } from '../../server.service';
import { DataService } from '../../data.service';
import { Response } from '@angular/http'; 
import { NgForm } from '@angular/forms';
import { User } from '../../models/user.model';
import { Category } from '../../models/category.model';
import { Ebook } from '../../models/ebook.model';
import { Language } from '../../models/language.model';

@Component({
  selector: 'app-add-ebook',
  templateUrl: './add-ebook.component.html',
  styleUrls: ['./add-ebook.component.css']
})
export class AddEbookComponent implements OnInit {
  ebook: Ebook;
  id: string;
  oldEbook:Ebook;
  onlyRead:boolean;
  categories:Category[];
  languages:Language[];
  file:File;

  constructor(private route: ActivatedRoute, private router: Router, private serverService:ServerService, private dataService:DataService) { }

  ngOnInit() {
    this.categories = this.dataService.categories;
    this.onlyRead = true;
    this.ebook = new Ebook(-1,"","","",null,"","", new Category(-1,""), new Language(-1,""));
      
      this.serverService.getAllLanguages().subscribe(
              (response: Response) => {
                console.log(response);
                this.languages = response.json();
              }
          );
  }

   onSubmit(){
    console.log("do add");
    this.ebook.categoryEntity.id = this.getIdFromCategoryName(this.ebook.categoryEntity.name);
    this.ebook.languageEntity.id = this.getIdFromLanguageName(this.ebook.languageEntity.name);

    this.serverService.addEbook(this.ebook).subscribe(
      (response: Response) => {
        if(response.statusText === "OK"){
             toastr.success("Success add");
             let tempEbook = response.json();
             if(this.dataService.selectedCategory === "All" || this.dataService.selectedCategory === this.ebook.categoryEntity.name){
               this.dataService.ebooks.push(this.cloneEbook(tempEbook));
             }
             for (var i = this.categories.length - 1; i >= 0; i--) {
               if(this.dataService.categories[i].name === this.ebook.categoryEntity.name){
                 this.dataService.categories[i].eBookEntityList.push(this.cloneEbook(tempEbook));
                 break;
               }
             }
             this.router.navigate(['../'], { relativeTo: this.route});
         }
      },error => {
            toastr.error("Failed add");
        }
     );
    
    
  }

  uploadEbook(){
    console.log(this.file);
    this.serverService.uploadFile(this.file).subscribe(
              (response: Response) => {
                console.log(response);
                console.log("Uspesno uploadovan file");
                let responseEbook: Ebook = response.json();
                this.ebook.fileName = responseEbook.fileName;
                this.ebook.author = responseEbook.author;
                this.ebook.title = responseEbook.title;
                this.ebook.keywords = responseEbook.keywords;
                this.ebook.mime = responseEbook.mime;
              }
          );
  }

  getIdFromCategoryName(categoryName:string){
    for (var i = this.categories.length - 1; i >= 0; i--) {
      if(this.categories[i].name === categoryName){
        return this.categories[i].id;
      }
    }
  }

  getIdFromLanguageName(languageName:string){
    for (var i = this.languages.length - 1; i >= 0; i--) {
      if(this.languages[i].name === languageName){
        return this.languages[i].id;
      }
    }
  }

  refresh(){
    this.ebook = this.cloneEbook(this.oldEbook);
  }

  cloneEbook(ebook:Ebook){
    return new Ebook(ebook.id, ebook.title, ebook.author, ebook.keywords, ebook.publicationYear, ebook.fileName, ebook.mime, new Category(ebook.categoryEntity.id, ebook.categoryEntity.name), new Language(ebook.languageEntity.id, ebook.languageEntity.name));
  }

  fileEvent(e){
    //console.log(e.target.files[0]);

    this.file = e.target.files[0];
    console.log(this.file);
    //this.ebook.title = this.file.name;
  }

  checkUpload(){
    return this.file == undefined;
  }

}

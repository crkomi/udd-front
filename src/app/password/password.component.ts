import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { PasswordJson } from './password-json.model';
import  { ServerService } from '../server.service';

@Component({
	selector : 'app-password',
  styleUrls: ['./password.component.css'],
	templateUrl: './password.component.html'
})
export class PasswordComponent implements OnInit {

  passwordJson:PasswordJson;
  
  constructor(private router: Router, private serverService:ServerService) {}

  ngOnInit() {
    this.passwordJson = new PasswordJson(this.serverService.currentUser.id,"","","",false);
  }

  onFetchData(){
    
  }

  onLogout(){
    
  }
  onSubmit(){
    console.log("do edit");
    if(this.passwordJson.newPassword != this.passwordJson.repeatNewPassword){
       toastr.info("New and repeat new passwor must be equals!");
       return;
    }
    this.serverService.changePassword(this.passwordJson).subscribe(
           (response: Response) => {
             let responsePasswordJson:PasswordJson = response.json();
              console.log(response.json());
              if(response.json().status){
                   toastr.success("Succes change password");
              }else{
                   toastr.error("Incorrect change password");
              }
           }
       );
    
  }

}
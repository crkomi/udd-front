
export class PasswordJson{

    public  userId:number;
    public oldPassword:string;
    public newPassword:string;
    public repeatNewPassword:string;
    public status:boolean;

	constructor(userId:number, oldPassword:string, newPassword:string, repeatNewPassword:string, status:boolean){
		this.userId = userId;
		this.oldPassword = oldPassword;
		this.newPassword = newPassword;
		this.repeatNewPassword = repeatNewPassword;
		this.status = status;
	}
	
}
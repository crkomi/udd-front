import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ServerService } from '../../server.service';
import { DataService } from '../../data.service';
import { Response } from '@angular/http'; 
import { NgForm } from '@angular/forms';
import { User } from '../../models/user.model';
import { Category } from '../../models/category.model';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {
  user: User;
  id: string;
  oldUser:User;
  onlyRead:boolean;
  categories:Category[];

  constructor(private route: ActivatedRoute, private router: Router, private serverService:ServerService, private dataService:DataService) { }

  ngOnInit() {
    this.onlyRead = true;
    this.user = new User(-1, "","","","","", new Category(-1,""));
      this.serverService.getAllCategories().subscribe(
              (response: Response) => {
                console.log(response);
                this.categories = response.json();
              }
          );
  }

   onSubmit(){
    console.log("do add");
    this.user.categoryEntity.id = this.getIdFromCategoryName(this.user.categoryEntity.name);

    this.serverService.addUser(this.user).subscribe(
      (response: Response) => {
        console.log("Success edit");
          if(response.statusText === "OK"){
             toastr.success("Success add");
             if(this.user.type === "Admin"){
                  this.dataService.usersAdministrators.push(this.cloneUser(response.json()));
                }else{
                  this.dataService.usersSubscriptions.push(this.cloneUser(response.json()));
                } 
             this.router.navigate(['../'], { relativeTo: this.route});
           }
      },error => {
            toastr.error("Incorrect username");
        }
     );
    
  }

  getIdFromCategoryName(categoryName:string){
    for (var i = this.categories.length - 1; i >= 0; i--) {
      if(this.categories[i].name === categoryName){
        return this.categories[i].id;
      }
    }
  }

  refresh(){
    this.user = new User(-1, "","","","","", new Category(-1,""));
  }

  cloneUser(user:User){

    return new User(user.id, user.firstname, user.lastname, user.username, user.password, user.type, new Category(user.categoryEntity.id, user.categoryEntity.name));
  }



}

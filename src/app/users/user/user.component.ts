import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ServerService } from '../../server.service';
import { DataService } from '../../data.service';
import { Response } from '@angular/http'; 
import { NgForm } from '@angular/forms';
import { User } from '../../models/user.model';
import { Category } from '../../models/category.model';


@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User;
  id: string;
  oldUser:User;
  onlyRead:boolean;
  categories:Category[];

  constructor(private route: ActivatedRoute, private router: Router, private serverService:ServerService, private dataService:DataService) { }

  ngOnInit() {
    this.user = new User(-1, "","","","","", new Category(-1,""));
      this.route.params.subscribe(
        (params: Params) => {
           this.id = params['id'];

           console.log(this.id);
           this.serverService.getUser(this.id).subscribe(
              (response: Response) => {
                console.log(response);
                this.user = response.json();
                this.oldUser =  this.cloneUser(this.user);
                this.onlyRead = false;
              }
          );
      });
      this.serverService.getAllCategories().subscribe(
              (response: Response) => {
                console.log(response);
                this.categories = response.json();
              }
          );
  }

   onSubmit(){
    console.log("do edit");
    this.user.categoryEntity.id = this.getIdFromCategoryName(this.user.categoryEntity.name);

    this.serverService.editUser(this.user).subscribe(
      (response: Response) => {
        console.log("Success edit");
          if(response.statusText === "OK"){
            this.oldUser =  this.cloneUser(this.user)
              if(this.user.type === "Admin"){
                for (var i = this.dataService.usersAdministrators.length - 1; i >= 0; i--) {
                  if(this.dataService.usersAdministrators[i].id === this.user.id){
                    this.dataService.usersAdministrators[i] = response.json();
                  }
                }
              }else{
                for (var i = this.dataService.usersSubscriptions.length - 1; i >= 0; i--) {
                  if(this.dataService.usersSubscriptions[i].id === this.user.id){
                    this.dataService.usersSubscriptions[i] = response.json();
                  }
                }
              }
              
              toastr.success("Success edit"); 
           }else{
              
           }
      },error => {
            toastr.error("Incorrect username");
        }
     );
    
  }

  getIdFromCategoryName(categoryName:string){
    for (var i = this.categories.length - 1; i >= 0; i--) {
      if(this.categories[i].name === categoryName){
        return this.categories[i].id;
      }
    }
  }

  refresh(){
    this.user = this.cloneUser(this.oldUser);
  }

  cloneUser(user:User){

    return new User(user.id, user.firstname, user.lastname, user.username, user.password, user.type, new Category(user.categoryEntity.id, user.categoryEntity.name));
  }

  delete(){
    this.serverService.deleteUser(this.user.id).subscribe(
      (response: Response) => {
        console.log("Success delete");
          if(response.statusText === "OK"){
            toastr.success("Success delete");
            if(this.user.type === "Admin"){
              for (var i = this.dataService.usersAdministrators.length - 1; i >= 0; i--) {
                if(this.dataService.usersAdministrators[i].username === this.user.username){
                  this.dataService.usersAdministrators.splice(i,1);
                  break;
                }
              }
            }else{
              for (var i = this.dataService.usersSubscriptions.length - 1; i >= 0; i--) {
                if(this.dataService.usersSubscriptions[i].username === this.user.username){
                  this.dataService.usersSubscriptions.splice(i,1);
                  break;
                }
              }
            }  
            this.router.navigate(['../'], { relativeTo: this.route});
           }else{
              
           }
      },error => {
            toastr.error("Failed delete");
        }
     );
  }



}

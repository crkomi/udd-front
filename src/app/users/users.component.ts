import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import  { ServerService } from '../server.service';
import  { DataService } from '../data.service';

@Component({
	selector : 'app-users',
  styleUrls: ['./users.component.css'],
	templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {

  usersAdministrators:User[];
  usersSubscriptions:User[];
  
  constructor(private router: Router, private serverService:ServerService, private dataService:DataService) {}

  ngOnInit() {
    this.usersAdministrators = this.dataService.usersAdministrators;
    this.usersSubscriptions = this.dataService.usersSubscriptions;
    this.usersAdministrators.splice(0, this.usersAdministrators.length);
    this.usersSubscriptions.splice(0, this.usersSubscriptions.length);
    
    this.serverService.getAllUsers().subscribe(
           (response: Response) => {
             let responseUsers:User[] = response.json();
              console.log(response.json());
              for (var i = responseUsers.length - 1; i >= 0; i--) {
                if(responseUsers[i].type === "Admin"){
                  this.usersAdministrators.push(responseUsers[i]);
                }else{
                  this.usersSubscriptions.push(responseUsers[i]);
                }
              }
           }
       );
  }

  onFetchData(){
    
  }

  onLogout(){
    
  }
  onSubmit(){
    console.log("do edit");
    
    
  }

  refreshTree(){
    this.usersAdministrators.splice(0, this.usersAdministrators.length);
    this.usersSubscriptions.splice(0, this.usersSubscriptions.length);
    this.serverService.getAllUsers().subscribe(
           (response: Response) => {
             let responseUsers:User[] = response.json();
              console.log(response.json());
              for (var i = responseUsers.length - 1; i >= 0; i--) {
                if(responseUsers[i].type === "Admin"){
                  this.usersAdministrators.push(responseUsers[i]);
                }else{
                  this.usersSubscriptions.push(responseUsers[i]);
                }
              }
           }
       );
  }
}
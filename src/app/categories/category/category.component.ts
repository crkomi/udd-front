import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ServerService } from '../../server.service';
import { DataService } from '../../data.service';
import { Response } from '@angular/http'; 
import { NgForm } from '@angular/forms';
import { User } from '../../models/user.model';
import { Category } from '../../models/category.model';


@Component({
  selector: 'app-catagory',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  category: Category;
  id: string;
  oldCategory:Category;
  onlyRead:boolean;

  constructor(private route: ActivatedRoute, private router: Router, private serverService:ServerService, private dataService:DataService) { }

  ngOnInit() {
    this.category = new Category(-1,"");
      this.route.params.subscribe(
        (params: Params) => {
           this.id = params['id'];

           console.log(this.id);
           this.serverService.getCategory(this.id).subscribe(
              (response: Response) => {
                console.log(response);
                this.category = response.json();
                this.oldCategory =  this.cloneCategory(this.category);
                this.onlyRead = false;
              }
          );
      });
  }

   onSubmit(){
    console.log("do edit");
    
    this.serverService.editCategory(this.category).subscribe(
      (response: Response) => {
        console.log("Success edit");
          if(response.statusText === "OK"){
            this.oldCategory =  this.cloneCategory(this.category)
            for (var i = this.dataService.categories.length - 1; i >= 0; i--) {
               if(this.dataService.categories[i].id == this.category.id){
                 this.dataService.categories[i] = response.json();
               }
            }
              toastr.success("Success edit"); 
           }else{
              
           }
      },error => {
            toastr.error("Incorrect name");
        }
     );
    
  }

  refresh(){
    this.category = this.cloneCategory(this.oldCategory);
  }

  cloneCategory(category:Category){
    return new Category(category.id, category.name);
  }

  delete(){
    this.serverService.deleteCategory(this.category.id).subscribe(
      (response: Response) => {
        console.log("Success delete");
          if(response.statusText === "OK"){
            toastr.success("Success delete");
            for (var i = this.dataService.categories.length - 1; i >= 0; i--) {
                if(this.dataService.categories[i].name === this.category.name){
                  this.dataService.categories.splice(i,1);
                  break;
                } 
            }  
            this.router.navigate(['../'], { relativeTo: this.route});
           }else{
              
           }
      },error => {
            toastr.error("Failed delete");
        }
     );
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ServerService } from '../../server.service';
import { DataService } from '../../data.service';
import { Response } from '@angular/http'; 
import { NgForm } from '@angular/forms';
import { User } from '../../models/user.model';
import { Category } from '../../models/category.model';


@Component({
  selector: 'app-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.css']
})
export class AddCategoryComponent implements OnInit {
  category: Category;
  id: string;
  oldCategory:Category;
  onlyRead:boolean;
  
  constructor(private route: ActivatedRoute, private router: Router, private serverService:ServerService, private dataService:DataService) { }

  ngOnInit() {
    this.onlyRead = true;
    this.category = new Category(-1,"");
  }

   onSubmit(){
    console.log("do add");
    
    this.serverService.addCategory(this.category).subscribe(
      (response: Response) => {
        console.log("Success edit");
          if(response.statusText === "OK"){
             toastr.success("Success add");
             this.dataService.categories.push(response.json()); 
             this.router.navigate(['../'], { relativeTo: this.route});
           }
      },error => {
            toastr.error("Incorrect name");
        }
     );
    
  }

  

  refresh(){
    this.category = new Category(-1,"");
  }

  cloneCategory(category:Category){
    return new Category(category.id, category.name);
  }



}

import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { Category } from '../models/category.model';
import  { ServerService } from '../server.service';
import  { DataService } from '../data.service';

@Component({
	selector : 'app-categories',
  styleUrls: ['./categories.component.css'],
	templateUrl: './categories.component.html'
})
export class CategoriesComponent implements OnInit {

  categories:Category[];
  
  constructor(private router: Router, private serverService:ServerService, private dataService:DataService) {}

  ngOnInit() {
    this.categories = this.dataService.categories;
    this.categories.splice(0, this.categories.length);
    this.serverService.getAllCategories().subscribe(
           (response: Response) => {
             let responseCategories:Category[] = response.json();
              console.log(response.json());
              for (var i = responseCategories.length - 1; i >= 0; i--) {
                this.categories.push(responseCategories[i]);
              }
           }
       );
  }

  onFetchData(){
    
  }

  onLogout(){
    
  }

  onSubmit(){  
  }

  refreshTree(){
    this.categories.splice(0, this.categories.length);
    this.serverService.getAllCategories().subscribe(
           (response: Response) => {
             let responseCategories:Category[] = response.json();
              console.log(response.json());
              for (var i = responseCategories.length - 1; i >= 0; i--) {
                this.categories.push(responseCategories[i]);
              }
           }
       );
  }
}
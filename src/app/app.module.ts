import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';

import { HeaderComponent} from './header/header.component';

import { AppRoutingModule } from './app-routing.module';

import { LoginComponent } from './login/login.component';

import { ProfileComponent } from './profile/profile.component';

import { ServerService} from './server.service';

import { DataService} from './data.service';

import { HomeComponent } from './home/home.component';

import { PasswordComponent } from './password/password.component';

import { UsersComponent } from './users/users.component';

import { CategoriesComponent } from './categories/categories.component';

import { UserComponent } from './users/user/user.component';

import { EbookComponent } from './ebooks/ebook/ebook.component';

import { EbooksComponent } from './ebooks/ebooks.component';

import { CategoryComponent } from './categories/category/category.component';

import { AddUserComponent } from './users/adduser/add-user.component';

import { AddCategoryComponent } from './categories/addcategory/add-category.component';

import { AddEbookComponent } from './ebooks/addebook/add-ebook.component';

import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [
    HeaderComponent,
    LoginComponent,
    HomeComponent,
    ProfileComponent,
    PasswordComponent,
    UsersComponent,
    UserComponent,
    AddUserComponent,
    CategoriesComponent,
    CategoryComponent,
    AddCategoryComponent,
    EbooksComponent,
    EbookComponent,
    AddEbookComponent,
    SearchComponent,
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    ServerService,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Injectable } from '@angular/core';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import { User } from './models/user.model';
import { Category } from './models/category.model';
import { Ebook } from './models/ebook.model';
import { PasswordJson } from './password/password-json.model';
import { SimpleQuery } from './models/simple-query.model'
import { AdvancedQuery } from './models/advanced-query.model';


@Injectable()
export class ServerService {

  private  urlUPPBackend = 'http://localhost:8080/api';
  public currentUser:User;
  public isLoggedUser:boolean;

  constructor(private http: Http) {
      this.currentUser = new User(-1, "","","","","", new Category(-1,""));
      this.isLoggedUser = false;
  }

  login(user:User){
    return this.http.post(this.urlUPPBackend + "/users/login", user);
  }

  isAdmin(){
    if(this.currentUser.type === "Admin"){
      return true;
    }else{
      return false;
    }
  }

  isSubscriber(){
    if(this.currentUser.type === "Subscriber"){
      return true;
    }else{
      return false;
    }
  }

  editUser(user:User){
    return this.http.put(this.urlUPPBackend + "/users/" + user.id, user);
  }

  editCategory(category:Category){
    return this.http.put(this.urlUPPBackend + "/categories/" + category.id, category);
  }

  editEbook(ebook:Ebook){
    return this.http.put(this.urlUPPBackend + "/ebooks/" + ebook.id, ebook);
  }

  addUser(user:User){
    return this.http.post(this.urlUPPBackend + "/users", user);
  }

  addCategory(category:Category){
    return this.http.post(this.urlUPPBackend + "/categories", category);
  }

  addEbook(ebook:Ebook){
    return this.http.post(this.urlUPPBackend + "/ebooks", ebook);
  }

  changePassword(passwordJson:PasswordJson){
    return this.http.post(this.urlUPPBackend + "/users/changepassword", passwordJson);
  }

  getAllUsers(){
    return this.http.get(this.urlUPPBackend + "/users");
  }

  getUser(userId:string){
    return this.http.get(this.urlUPPBackend + "/users/" + userId);
  }

  getAllCategories(){
    return this.http.get(this.urlUPPBackend + "/categories");
  }

  getAllLanguages(){
    return this.http.get(this.urlUPPBackend + "/languages");
  }

  getCategory(categoryId:string){
    return this.http.get(this.urlUPPBackend + "/categories/" + categoryId);
  }

  deleteUser(id:number){
    return this.http.delete(this.urlUPPBackend + "/users/" + id);
  }

  deleteCategory(id:number){
    return this.http.delete(this.urlUPPBackend + "/categories/" + id);
  }

  deleteEbook(id:number){
    return this.http.delete(this.urlUPPBackend + "/ebooks/" + id);
  }

  getEbooks(categoryId:number){
    return this.http.get(this.urlUPPBackend + "/categories/" + categoryId + "/ebooks");
  }

  getBook(id:string){
    return this.http.get(this.urlUPPBackend + "/ebooks/" + id);
  }

  uploadFile(filedate:any){
    //return this.http.post(this.urlUPPBackend + "/users/upload", filedate);
    let file: File = filedate;
    let formData:FormData = new FormData();
    formData.append('file', file, file.name);
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.urlUPPBackend + "/ebooks/upload", formData,options);
  }

  sendQueryParser(simpleQuery:SimpleQuery){
    if(simpleQuery.field == ""){
      simpleQuery.field = "title";
    }
    return this.http.post(this.urlUPPBackend + "/search/queryParser", simpleQuery);
  }

  sendFuzzy(simpleQuery:SimpleQuery){
    if(simpleQuery.field == ""){
      simpleQuery.field = "title";
    }
    return this.http.post(this.urlUPPBackend + "/search/fuzzy", simpleQuery);
  }
  
  sendPhrase(simpleQuery:SimpleQuery){
    if(simpleQuery.field == ""){
      simpleQuery.field = "title";
    }
    return this.http.post(this.urlUPPBackend + "/search/phrase", simpleQuery);
  }

  sendTerm(simpleQuery:SimpleQuery){
    if(simpleQuery.field == ""){
      simpleQuery.field = "title";
    }
    return this.http.post(this.urlUPPBackend + "/search/term", simpleQuery);
  }
  
  sendBoolean(advancedQuery:AdvancedQuery){
    if(advancedQuery.field1 == ""){
      advancedQuery.field1 = "title";
    }
    if(advancedQuery.field2 == ""){
      advancedQuery.field2 = "title";
    }
    return this.http.post(this.urlUPPBackend + "/search/boolean", advancedQuery);
  }

}
import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { Category } from '../models/category.model';
import { Language } from '../models/language.model';
import  { ServerService } from '../server.service';
import { Ebook } from '../models/ebook.model';

@Component({
	selector : 'app-search',
  styleUrls: ['./search.component.css'],
	templateUrl: './search.component.html'
})
export class SearchComponent implements OnInit {

  ebooks:Ebook[];
  field:string;
  value:string;
  field1:string;
  value1:string;
  searchType:string;
  operation:string;

  
  constructor(private router: Router, private serverService:ServerService) {}

  ngOnInit() {
    this.field = "title";
    this.value = "";
    this.field1 = "title";
    this.value1 = "";
    this.searchType = "";
    this.operation = "or";
    //let e:Ebook = new Ebook(-1,"","","",1,"","", new Category(-1,""), new Language(-1,""));
   // e.highlight = "test\ntstsdgfsfsdafsdfsdgsdfgfdsgfdgfdsgfsdgfdgfdsgsdgdsfg\nsdfdsfssdfdsfsdfsdfsdfsdfsdfsdfdsftest\ntstsdgfsfsdafsdfsdgsdfgfdsgfdgfdsgfsdgfdgfdsgsdgdsfg\nsdfdsfssdfdsfsdfsdfsdfsdfsdfsdfdsftest\ntstsdgfsfsdafsdfsdgsdfgfdsgfdgfdsgfsdgfdgfdsgsdgdsfg\nsdfdsfssdfdsfsdfsdfsdfsdfsdfsdfdsf";
    this.ebooks = [];//new Ebook(-1,"","","",1,"","", new Category(-1,""), new Language(-1,""));
    //this.ebooks.push(new Ebook(-1,"test","test","test",1,"test","test", new Category(-1,"test"), new Language(-1,"test")));
    //this.ebooks.push(new Ebook(-1,"test","test","test",1,"test","test", new Category(-1,"test"), new Language(-1,"test")));
    //this.ebooks.push(e);
  }

  onFetchData(){
    
  }

  isDownloadPdf(ebook:Ebook){
        if(this.serverService.isAdmin() || ( this.serverService.isSubscriber() && (this.serverService.currentUser.categoryEntity.name === ebook.categoryEntity.name || this.serverService.currentUser.categoryEntity.name == "All"))){
          return true;
        }
        return false;
  }

  clickSearch(){
    console.log("Field: " + this.field);
    console.log("Search Type: " + this.searchType);
    console.log("Value: " + this.value);
    if(this.searchType === "QueryParser"){
      console.log("QUERYPARSER");
      this.serverService.sendQueryParser({field:this.field, value:this.value}).subscribe(
              (response: Response) => {
                console.log(response);
                let newEbooks:Ebook[] = response.json();
                this.ebooks.splice(0, this.ebooks.length);
                for (var i = newEbooks.length - 1; i >= 0; i--) {
                  let tempArray:string[] = newEbooks[i].fileName.split('\\');
                  newEbooks[i].ebookDownloadLink = "http://localhost:8080/" + tempArray[tempArray.length - 1];
                  this.ebooks.push(newEbooks[i]);
                }
              }
          );
    }else if(this.searchType == "Boolean"){
      console.log("BOOLEAN");
      this.serverService.sendBoolean({field1:this.field, value1:this.value,field2:this.field1, value2:this.value1, operation:this.operation}).subscribe(
              (response: Response) => {
                console.log(response);
                let newEbooks:Ebook[] = response.json();
                this.ebooks.splice(0, this.ebooks.length);
                for (var i = newEbooks.length - 1; i >= 0; i--) {
                  let tempArray:string[] = newEbooks[i].fileName.split('\\');
                  newEbooks[i].ebookDownloadLink = "http://localhost:8080/" + tempArray[tempArray.length - 1];
                  this.ebooks.push(newEbooks[i]);
                }
              }
          );
    }else if(this.searchType === "Phrase"){
      console.log("Phrase");
      this.serverService.sendPhrase({field:this.field, value:this.value}).subscribe(
              (response: Response) => {
                console.log(response);
                let newEbooks:Ebook[] = response.json();
                this.ebooks.splice(0, this.ebooks.length);
                for (var i = newEbooks.length - 1; i >= 0; i--) {
                  let tempArray:string[] = newEbooks[i].fileName.split('\\');
                  newEbooks[i].ebookDownloadLink = "http://localhost:8080/" + tempArray[tempArray.length - 1];
                  this.ebooks.push(newEbooks[i]);
                }
              }
          );
    }else if(this.searchType === "Fuzzy"){
      console.log("FUZZY");
      this.serverService.sendFuzzy({field:this.field, value:this.value}).subscribe(
              (response: Response) => {
                console.log(response);
                let newEbooks:Ebook[] = response.json();
                this.ebooks.splice(0, this.ebooks.length);
                for (var i = newEbooks.length - 1; i >= 0; i--) {
                  let tempArray:string[] = newEbooks[i].fileName.split('\\');
                  newEbooks[i].ebookDownloadLink = "http://localhost:8080/" + tempArray[tempArray.length - 1];
                  this.ebooks.push(newEbooks[i]);
                }
              }
          );
    }else if(this.searchType === "Term"){
      console.log("FUZZY");
      this.serverService.sendTerm({field:this.field, value:this.value}).subscribe(
              (response: Response) => {
                console.log(response);
                let newEbooks:Ebook[] = response.json();
                this.ebooks.splice(0, this.ebooks.length);
                for (var i = newEbooks.length - 1; i >= 0; i--) {
                  let tempArray:string[] = newEbooks[i].fileName.split('\\');
                  newEbooks[i].ebookDownloadLink = "http://localhost:8080/" + tempArray[tempArray.length - 1];
                  this.ebooks.push(newEbooks[i]);
                }
              }
          );
    }
  }


 
}
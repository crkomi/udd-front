import { Injectable } from '@angular/core';
import { User } from './models/user.model';
import { Category } from './models/category.model';
import { Ebook } from './models/ebook.model';

@Injectable()
export class DataService {

  public usersAdministrators:User[];
  public usersSubscriptions:User[];
  public categories:Category[];
  public ebooks:Ebook[];
  public selectedCategory:string;

  constructor() {
      this.usersAdministrators = [];
      this.usersSubscriptions = [];
      this.categories = [];
      this.ebooks = [];
      this.selectedCategory = "";
  }

}
import  {Component, OnInit} from '@angular/core';
import { Response} from '@angular/http';
import { Router } from '@angular/router';
import  { ServerService } from '../server.service';
import { User } from '../models/user.model';
import { Category } from '../models/category.model';


@Component({
	selector : 'app-header',
	templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  
  constructor(private serverService:ServerService, private router:Router) {

  }

  ngOnInit() {
    this.serverService.currentUser = new User(-1, "","","","","", new Category(-1,""));
  }

  onLogout(){
    this.serverService.currentUser = new User(-1, "","","","","", new Category(-1,""));
    this.router.navigateByUrl('/login');
    this.serverService.isLoggedUser = false;
  }

  onSaveData(){
   
  }

}

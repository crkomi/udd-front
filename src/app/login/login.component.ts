import { Component, OnInit } from '@angular/core';
import { Response} from '@angular/http';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../models/user.model';
import { Category } from '../models/category.model';
import  { ServerService } from '../server.service';

@Component({
	selector : 'app-login',
  styleUrls: ['./login.component.css'],
	templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  user:User;
  
  constructor(private router: Router, private serverService:ServerService) {}

  ngOnInit() {
    this.user = this.user = new User(-1, "","","","","", new Category(-1,""));
  }

  onSubmit() {
    console.log("username: " + this.user.username + " password: " + this.user.password);
    this.serverService.login(this.user).subscribe(
           (response: Response) => {
              let responseUser:User = response.json();
              if(response.statusText === "OK"){
                this.serverService.isLoggedUser = true;
                this.serverService.currentUser = responseUser;
                //toastr.success("Sucess login");
                this.router.navigateByUrl('home');
                toastr.success("Successs login");
              }
           },error => {
             this.serverService.isLoggedUser = false;
               this.serverService.currentUser = new User(-1, "","","","","", new Category(-1,""));
               toastr.error("Error login");
          }
       );
  }

}